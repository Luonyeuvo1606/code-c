#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

class Point
{
	private:
		float x,y;
	public:
		Point(){
		}
		~Point(){
		}
		void InPut();
		void OutPut();
		float TinhKhoangCach(Point b);
};
void Point::InPut()
{
	cout<<"Nhap vao toa do x: ";cin>>x;
	cout<<"Nhap vao toa do y: ";cin>>y;
}
void Point::OutPut()
{
	cout<<"Toa do: "<<"("<<x<<","<<y<<")";
}
float Point::TinhKhoangCach(Point b)
{
	return (sqrt(pow(x-b.x,2.0)+pow(y-b.y,2.0)));
}
int main()
{	vector <float> X;
	cout<<"Nhap vao so diem: ";
	int n;
	cin>>n;
	Point A[n];
	for(int i=0;i<n;i++)
	{
		cout<<"Nhap vao diem "<<i+1<<": "<<endl;
		A[i].InPut();
	}
	for(int i=0;i<n-1;i++)
	{	for(int j=i+1 ;j<n;j++)
		 {
		 	X.push_back(A[i].TinhKhoangCach(A[j]));
		 }
	}
	float max = X[0];
	for(int i=1;i<X.size();i++)
	{
		if(X[i] > max)
		{
			max = X[i];
		}
	}
	cout<<"Khoang cach giua hai diem trong n diem lon nhat la: "<<max;
}
