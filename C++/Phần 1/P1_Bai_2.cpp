#include <iostream>
#include <math.h>

using namespace std;

class tamgiac
{
private:
	double a, b, c, p;
public:
	void nhap() {
		do {
			cout << "Nhap vao canh a: ";
			cin >> a;
			cout << "Nhap vao canh b: ";
			cin >> b;
			cout << "Nhap vao canh c: ";
			cin >> c;
		} while (a + b <= c || a + c <= b || c + b <= a);
	};

	double dientich() {
		p = (a + b + c) / 2;
		return sqrt(p * (p - a) + p * (p - b) + p * (p - c));
	};

	void kiemtra() {
		cout << "Tinh chat tam giac: ";
		if (a == b && a == c)
			cout << "Tam giac deu";
		else if (a != b && a != c && b != c && (pow(a, 2) + pow(b, 2) == pow(c, 2) || pow(a, 2) + pow(c, 2) == pow(b, 2) || pow(c, 2) + pow(b, 2) == pow(a, 2)))
			cout << "Tam giac vuong";
		else if (pow(a, 2) + pow(b, 2) == pow(c, 2) || pow(a, 2) + pow(c, 2) == pow(b, 2) || pow(c, 2) + pow(b, 2) == pow(a, 2))
			cout << "Tam giac vuong can";
		else if (a != b && a != c && b != c)
			cout << "Tam giac thuong";
		else
			cout << "Tam giac can";
	};

	void in() {
		cout << "Dien tich tam giac la: " << dientich() << endl;
		kiemtra();
	};
};

void main()
{
	tamgiac tg;
	tg.nhap();
	tg.in();
}