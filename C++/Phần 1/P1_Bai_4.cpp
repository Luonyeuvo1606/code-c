﻿#include<iostream>
#include<stdio.h>
#include<string.h>
#define n 1000

using namespace std;

class String {
	char* str;
	int length = 0;
public:
	//Hàm tạo
	String() {
		this->str;
	};
	String(char* a) {
		this->str = a;
		this->length = strlen(a);
	}

	//Hàm hủy
	~String() {
		this->str;
	}
	//Hàm nhập
	void Nhap() {
		char s[n];
		cout << "Nhap xau ki tu: ";
		cin >> s;
		this->str = s;
		this->length = strlen(s);
	}
	//Hàm hiển thị
	void Xuat() {
		cout << "Xau co dang: " << str << endl;
		cout << "Do dai xau la: " << length << endl;
	}
	//Hàm nối để cộng 2 xâu
	void Noi(String s) {
		int a = this->length + s.length;
		char* st = new char[a];
		strcpy(st, this->str);
		strcat(st, s.str);

		this->str = st;
		this->length = a;
	}
};

int main() {
	char s[] = "Hello world";
	String s1;
	s1.Nhap();
	s1.Xuat();
	String s2(s);
	s2.Xuat();
	s1.Noi(s2);
	cout << "Xau sau khi noi la:" << endl;
	s1.Xuat();
}