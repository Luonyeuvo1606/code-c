﻿#include<iostream>
using namespace std;

class PhanSo {
private:
    int ts, ms;
public:
    PhanSo() {}
    PhanSo(int ts, int ms) {
        this->ts = ts;
        this->ms = ms;
    }
    ~PhanSo() {}

    friend istream& operator>>(istream& nhap, PhanSo& A) {
        cout << "Nhap tu so: "; nhap >> A.ts;
        cout << "Nhap mau so: "; nhap >> A.ms;
        return nhap;
    }

    friend ostream& operator<<(ostream& xuat, PhanSo A) {
        xuat << A.ts << "/" << A.ms << endl;
        return xuat;
    }

    PhanSo operator+(PhanSo a) {
        PhanSo x;
        x.ts = ts * a.ms + a.ts * ms;
        x.ms = ms * a.ms;

        return x;
    }

    PhanSo operator-(PhanSo a) {
        PhanSo x;
        x.ts = ts * a.ms - a.ts * ms;
        x.ms = ms * a.ms;

        return x;
    }

    PhanSo operator*(PhanSo a) {
        PhanSo x;
        x.ts = ts * a.ts;
        x.ms = ms * a.ms;

        return x;
    }

    PhanSo operator/(PhanSo a) {
        PhanSo x;
        x.ts = ts * a.ms;
        x.ms = ms * a.ts;

        return x;
    }
};

int main() {
    PhanSo a, b;
    cin >> a; cin >> b;
    cout << "phan so a: " << a << endl;
    cout << "phan so b: " << b << endl;

    PhanSo x, y, z, e;
    x = a + b;
    y = a - b;
    z = a * b;
    e = a / b;
    cout << "a+b: " << x << endl;
    cout << "a-b: " << y << endl;
    cout << "a*b: " << z << endl;
    cout << "a/b: " << e << endl;
}
