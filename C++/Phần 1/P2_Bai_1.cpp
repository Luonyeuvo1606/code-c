﻿#include<iostream>
#include<cmath>

using namespace std;

class Point {
	float x, y;

public:
	Point() { this->x = 0; this->y = 0; }
	Point(float x, float y) {
		this->x = x;
		this->y = y;
	}
	~Point() {}

	friend istream& operator>>(istream& nhap, Point& A) {
		cout << "Nhap x: "; nhap >> A.x;
		cout << "Nhap y: "; nhap >> A.y;
		return nhap;
	}

	friend ostream& operator<<(ostream& xuat, Point& A) {
		cout << "Toa do diem: (" << A.x << ", " << A.y << ")";
		return xuat;
	}

	friend float KhoangCach(Point a, Point b) {
		return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
	}
};

class Dagiac {
private:
	Point* a;
	int size;
public:
	Dagiac() {}
	Dagiac(int size) {
		a = new Point[size];
		this->size = size;
	}

	~Dagiac() {}

	friend istream& operator>>(istream& nhap, Dagiac& DG) {
		cout << "Nhap so toa do: "; nhap >> DG.size;
		DG.a = new Point[DG.size];
		for (int i = 0; i < DG.size; ++i)
			nhap >> DG.a[i];
		return nhap;
	}

	friend ostream& operator<<(ostream& xuat, Dagiac DG) {
		cout << "Cac toa do:\n";
		for (int i = 0; i < DG.size; ++i)
			xuat << DG.a[i] << endl;
		return xuat;
	}

	friend bool KiemTra(Dagiac DG) {
		if (DG.size != 3) return false;
		float a = KhoangCach(DG.a[0], DG.a[1]);
		float b = KhoangCach(DG.a[0], DG.a[2]);
		float c = KhoangCach(DG.a[1], DG.a[2]);
		if (a + b > c && a + c > b && b + c > a) return true;

		return false;
	}
	friend bool KiemTraCan(Dagiac DG) {
		bool Bl = true;
		for (int i = 0; i < DG.size - 1; ++i)
			for (int j = i + 1; j < DG.size - 1; ++j)
				Bl = (KhoangCach(DG.a[i], DG.a[j]) == KhoangCach(DG.a[i + 1], DG.a[j + 1])) && Bl;

		return Bl;
	}
};

int main() {
	Dagiac Dg;

	cin >> Dg;
	cout << Dg;

	if (KiemTra(Dg))
		cout << "Da giac nay la tam giac." << endl;
	if (KiemTraCan(Dg))
		cout << "Da giac nay la da giac deu." << endl;
}