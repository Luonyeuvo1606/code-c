#include <iostream>
#include <math.h>
#define PI 3.14159265359

using namespace std;

class hinhtron
{
private:
	double r;
public:
	void nhap() {
		cout << "Nhap vao ban kinh: ";
		cin >> r;
	};
	double chuvi() {
		return (2 * PI * r);
	};
	double dientich() {
		return (PI * pow(r, 2));
	};
	void in() {
		cout << "Chu vi hinh tron la: " << chuvi() << endl;
		cout << "Dien tich hinh tron la: " << dientich() << endl;
	};
};

void main()
{
	hinhtron ht;
	ht.nhap();
	ht.in();
}