﻿#include<iostream>
#include<string>

using namespace std;

class ThiSinh {
	double dToan = 0, dHoa = 0, dLy = 0;
	string SBD = "DDL";

public:
	static int SL;
	void Nhap() {
		cout << "Nhap diem Toan: "; cin >> dToan;
		cout << "Nhap diem Ly: "; cin >> dLy;
		cout << "Nhap diem Hoa: "; cin >> dHoa;
		SBD += to_string(SL);
		++SL;
	}

	void Xuat() {
		cout << "SBD: " << SBD << endl;
		cout << "Diem Toan: " << dToan << endl;
		cout << "Diem Ly: " << dLy << endl;
		cout << "Diem Hoa: " << dHoa << endl;
	}

	double TinhTong() {
		return (dToan + dLy + dHoa);
	}
};

class ListThiSinh {
	ThiSinh* TS;
	int n;
public:
	ListThiSinh() {}
	ListThiSinh(int a) {
		this->n = a;
	}

	void NhapDS() {
		cout << "Nhap so luong thi sinh: "; cin >> n;
		TS = new ThiSinh[n];
		for (int i = 0; i < n; ++i) {
			cout << "\nNhap thi sinh thu " << (i + 1) << ": " << endl;
			TS[i].Nhap();
		}
	}

	void XuatDS() {
		for (int i = 0; i < n; ++i) {
			cout << "\nThi sinh thu " << (i + 1) << ":\n";
			TS[i].Xuat();
		}
	}

	void SxDiem() {
		for (int i = 0; i < n; ++i) {
			for (int j = i + 1; j > n; ++j) {
				if (TS[i].TinhTong() < TS[j].TinhTong()) {
					ThiSinh temp = TS[i];
					TS[i] = TS[j];
					TS[j] = temp;
				}
			}
		}
	}


	void PhanLoai18() {
		const int a = 18;
		for (int i = 0; i < n; ++i)
			if (TS[i].TinhTong() >= a)
				TS[i].Xuat();
	}
};
int ThiSinh::SL = 100;

int main() {
	ListThiSinh list;

	list.NhapDS();
	cout << endl << "\nThong tin: " << endl;
	list.XuatDS();

	cout << endl << "\nSap xep thu tu tang dan: " << endl;
	list.SxDiem();
	list.XuatDS();

	cout << endl << "\nThi sinh co tong diem >= 18:\n";
	list.PhanLoai18();
}