#include<iostream>
#include<math.h>

using namespace std;

class POINT
{
private:
	int x, y;
public:
	POINT() {
		int x = 0, y = 0;
	}

	void Nhap() {
		cout << endl << "Nhap gia tri x: "; cin >> x;
		cout << "Nhap gia tri y: "; cin >> y;
	}

	void Hien() {
		cout << "Toa do diem (x,y) = (" << x << "," << y << ")";
	}

	double Khoangcach(POINT b);
	int getx() {
		return x;
	}
	int gety() {
		return y;
	}
	void setx(int a) {
		x = a;
	}
	void sety(int b) {
		y = b;
	}
};

double POINT::Khoangcach(POINT b) {
	return sqrt(pow(this->x - b.x, 2) + pow(this->y - b.y, 2));
}

void main() {
	POINT* p;
	int n;
	cout << "Nhap vao so luong diem can nhap: "; cin >> n;
	p = new POINT[n];
	for (int i = 0; i < n; i++) {
		cout << endl << endl << "Nhap vao diem thu " << i + 1 << ":";
		p[i].Nhap();
	}
	cout << endl << "Cac diem vua nhap la:";
	for (int i = 0; i < n; i++) {
		cout << endl << "Diem thu " << i + 1 << ":";
		p[i].Hien();
	}
	float max = -1;
	for (int i = 0; i < n; ++i) {
		for (int j = i + 1; j < n; ++j) {
			float r = p[i].Khoangcach(p[j]);
			if (max < 0) max = r;
			else if (r > max) max = r;
		}
	}
	cout << endl << "Khoang cach giua hai diem trong n diem lon nhat la: " << max;
}