﻿#include<iostream>
#include<string>
using namespace std;

class NhanVien {
private:
    int Tuoi = 0;
    string Ten;
public:
    // hàm nhập 1 nhan vien
    void input() {
        cin.ignore();
        cout << "Nhap ten nhan vien: "; getline(cin, Ten);
        cout << "Nhap tuoi: "; cin >> Tuoi;
    }

    // hàm xem thông tin
    void info() {
        cout << "Ten nhan vien: " << Ten << endl;
        cout << "Tuoi nhan vien: " << Tuoi << endl;
    }

    // hàm xet ve huu
    void Check() {
        if (Tuoi >= 60) {
            cout << "Da ve huu" << endl;
        }
        else {
            cout << "Chua ve huu" << endl;
        }
    }
};

class CongNhan : NhanVien {
private:
    long NgayCong = 0;
    long MucLuong = 0;
    string TayNghe;
public:
    int id = 0;
    void input() {
        cout << "Nhap muc luong: "; cin >> MucLuong;
        cout << "Nhap ngay cong: "; cin >> NgayCong;
    }

    // hàm xem thông tin
    void info() {
        cout << "Muc luong: " << MucLuong << endl;
        cout << "So ngay cong: " << NgayCong << endl;
        cout << "Luong nhan duoc: " << NgayCong * MucLuong << endl;
    }
};

class CanBo : NhanVien {
private:
    int HeSoLuong = 0;
    int PhuCap = 0;
    string TrinhDo;
public:
    int id = 1;
    void input() {
        cout << "Nhap he so luong: "; cin >> HeSoLuong;
        cout << "Nhap phu cap: "; cin >> PhuCap;
    }

    // hàm xem thông tin
    void info() {
        cout << "He so luong: " << HeSoLuong << endl;
        cout << "Phu cap: " << PhuCap << endl;
        cout << "Luong nhan duoc: " << HeSoLuong * 14500 + PhuCap << endl;
    }
};

class List {
private:
    NhanVien* nhanvien;
    CongNhan* congnhan;
    CanBo* canbo;
    int n;

public:
    int LC = 0;
    List() {}
    List(int n) {
        this->n = n;
    }

    // hàm nhập danh sách
    void Nhap() {

        nhanvien = new NhanVien[n];
        cout << "Nhap so luong nhan vien: "; cin >> n;
        if (LC == 0)
            congnhan = new CongNhan[n];
        else
            canbo = new CanBo[n];

        for (int i = 0; i < n; ++i) {
            cout << endl << "Nhap nhan vien thu " << (i + 1) << ":\n";
            cout << "Day la cong nhan hay can bo ???\n(0 - Cong nhan / 1 - Can bo)\nNhap lua chon: "; cin >> LC;
            if (LC == 0)
                congnhan[i].input();
            else {
                canbo[i].input();
            }

        }
    }

    // hàm xem thông tin danh sich các thí sinh
    void Xuat() {
        for (int i = 0; i < n; ++i) {
            cout << endl << "Nhan vien thu " << (i + 1) << ":\n";
            if (congnhan->id == 0)
                congnhan[i].info();
            else if (canbo->id == 1)
                canbo[i].info();
            else {}
        }
    }

    // hàm xem ds về hưu
    void veHuu() {
        for (int i = 0; i < n; ++i) {
            cout << endl << "Nhan vien thu " << (i + 1) << ": ";
            nhanvien[i].Check();
        }
    }
};

int main() {
    List NV;

    NV.Nhap();
    cout << "------------------------------------------\n";
    cout << "THONG TIN NHAN VIEN:\n";
    NV.Xuat();
    cout << "------------------------------------------\n";
    cout << "THONG TIN VE HUU:\n";
    NV.veHuu();
}
