//TRAN VAN VU 61TH4
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <vector>

using namespace std;

class String
{
	private:

		int length;
	public:
		char *str = new char[20];//XIN O VUNG HEAP MOT VUNG NHO CO KIEU DU LIEU LA CHAR
		String(){				 // PHUONG THUC KHOI TAO KHONG THAM SO DUOC DINH NGHIA LUON O TRONG CLASS
		}
		String(char*s,int a){   // PHUONG THUC KHOI TAO HAI THAM SO
			this->str = s;		//CON TRO THIS THAM CHIEU DEN STR
			this->length = a;   //CON TRO THIS THAM CHIEU DEN LENGTH
		}
		void get_String2()		//KHONG DUNG CON TRO THIS
		{
			cout<<"Xau 2: "<<str;
			cout<<"  Do dai xau 2: "<<length;
		}
//		void *get_String2()     //DUNG CON TRO THIS
//		{
//			cout<<"Xau 2: "<<this->str;
//			cout<<"  Do dai xau 2: "<<this->length;
//				}		
		~String()
		{
			delete str;							// HUY BO NHO DA CAP PHAT
		}
		void InPut(){
			cout<<"Nhap vao xau ki tu :";
			fflush(stdin);
			gets(str);
		}
		void OutPut()
		{
			cout<<"Xau 1: "<<str;
		   length = strlen(str);				//HAM IN RA DO DAI CHUOI
		   cout<<" Do dai xau 1 la: "<<length<<endl;
		}
		char NoiXau(String s)
		{
			strcat(str,s.str);					//HAM NOI CHUOI
		}
};
int main()
{	char xau[20] = "abcxyz";					//KHOI TAO BIEN XAU
	int dodai = strlen(xau);					//CHIEU DAI CUA BIEN XAU SAU KHI KHOI TAO
	String A;
	cout<<"Nhap vao chuoi 1: "<<endl;
	A.InPut();
	A.OutPut();
	String B(xau,dodai);						//KHAI BAO STR2 CO 2 THAM SO TRUYEN VAO
	B.get_String2();
	A.NoiXau(B);
	cout<<endl<<"Xau 1 sau khi noi chuoi: "<<endl;
	A.OutPut();
	
}
